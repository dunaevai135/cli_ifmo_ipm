﻿using System;
using MenuSample;
using task1;

namespace task2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var userIntrfase = new CommandLineInterface(new App(new Calc()), new ConsoleCalcIO());
            
            userIntrfase.PrintUsage();
            userIntrfase.UserCommunication();
            
            
        }
    }
}