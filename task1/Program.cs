﻿namespace task1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var userIntrfase = new CommandLineInterface(new App(new Calc()), new ConsoleCalcIO());
            userIntrfase.PrintUsage();
            userIntrfase.UserCommunication();           
        }
    }
}