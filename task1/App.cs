﻿using System.Collections.Generic;

namespace task1
{
    public class App
    {
        public List<double> Answrers;
        public List<string> Operators;
		public List<double> Operands;
        
        private double? LOperand = null;
        private double? ROperand = null;
        private string Operator = null;
        private readonly ICalc calc;

        public App(ICalc calc)
        {
            this.calc = calc;
            Answrers = new List<double>();
			Operands = new List<double>();
            Operators = new List<string>();
        }

        public bool TryConsumeOperand(string value, out double ret)
        {
            if (!double.TryParse(value, out ret))
            {
                return false;
            }
            
			Operands.Add(ret);
            
            if (LOperand == null)
            {
                LOperand = ret;
                Answrers.Add(ret);
                return true;
            }
            
            ROperand = ret;
            
            
            if (Operator == null || !calc.TryCalculate(Operator, out ret, (double) LOperand, (double) ROperand))
            {
                return false;
            }
            LOperand = ret;
            
            Answrers.Add(ret);
            return true;
        }
        
        public bool TrySetAnsverAsOperand(string value, out double ret)
        {
            int realValue;
            ret = default(double);
            if (!int.TryParse(value, out realValue))
            {
                return false;
            }

            if (Answrers.Count <= realValue || realValue < 0) return false;
            ret = Answrers[realValue];
            LOperand = ret;
            ROperand = null;
            Answrers.Add(ret);
            return true;

        }
        
        public bool TryConsumeOperator(string value)
        {
            if (!calc.ContainsOperator(value)) return false;
            Operators.Add(value);
            Operator = value;
            return true;

        }
        
    }
}