﻿using task1.Comands;

namespace task1
{
    public class CommandLineInterface : InterfaceUserInterface
    {
        
        private readonly App application;
        private readonly ICalcIO calcIO;
        private readonly ComandListInitializer list;
        public enum State { GetOperand, GetOperator, SetAnsver, Exit};

        public CommandLineInterface(App _app, ICalcIO _calcIO)
        {
            application = _app;
            calcIO = _calcIO;
            list = new ComandListInitializer();
        }

        public void PrintUsage()
        {
            calcIO.WriteLine("Usage:\n" +
                             "  when first symbol on line is ‘>’ – enter operand(number)\n" +
                             "  when first symbol on line is ‘@’ – enter operation\n" +
                             "  operation is one of ‘+’, ‘-‘, ‘/’, ‘*’\n" +
                             "    ‘#’ followed with number of evaluation step\n" +
				             "    ‘s’ to save in file\n" +
				             "    ‘l’ to load from file\n" +
                             "    ‘q’ to exit\n");
        }
        
        public void UserCommunication()
        {
            double result;
            string question;
            string readBuffer = "";
            var stateMashin = State.GetOperand;
            while (stateMashin != State.Exit)
            {
                switch (stateMashin)
                {
                    case State.SetAnsver:
                    {
                        if (!application.TrySetAnsverAsOperand(readBuffer, out result))
                        {
                            PrintErrorWrongInput();
                            stateMashin = State.GetOperator;
                            break;
                        }

                        WriteResult(result);
                        stateMashin = State.GetOperator;

                        break;
                    }
                    case State.GetOperand:
                    {
                        question = "> ";
                        if (TryCheckReadOnComand(question, ref readBuffer, ref stateMashin))
                        {
                            break;
                        }

                        if (!application.TryConsumeOperand(readBuffer, out result))
                        {
                            PrintErrorWrongInput();
                            break;
                        }

                        WriteResult(result);
                        stateMashin = State.GetOperator;
                        break;
                    }
                    case State.GetOperator:
                    {
                        question = "@ ";
                        if (TryCheckReadOnComand(question, ref readBuffer, ref stateMashin))
                        {
                            break;
                        }

                        if (!application.TryConsumeOperator(readBuffer))
                        {
                            PrintErrorWrongInput();
                            break;
                        }

                        stateMashin = State.GetOperand;
                        break;
                    }
                    default:
                        stateMashin = State.Exit;
                        break;
                }
            }
        }

        private bool TryCheckReadOnComand(string question, ref string readBuffer, ref State stateMachin)
        {
            calcIO.Write(question);
            readBuffer = calcIO.ReadString();
            foreach (var comand in list.comands)
            {
                if (comand.CheckBufferOnComand(readBuffer))
                {
					return comand.TryRun(ref readBuffer, ref stateMachin, application);
                }
            }
            
            return false;
        }
        
        public void PrintErrorWrongInput()
        {
            calcIO.Write("Wrong input. Try again.\n");
        }
        
        private void WriteResult(double result)
        {
            calcIO.WriteLine("[#" + (application.Answrers.Count-1) + "] = " + result);
        }
    }
}