﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

namespace task1
{
    public class Calc : ICalc
    {
        private readonly Dictionary<string, Func<double, double, double>> operators = new Dictionary<string, Func<double, double, double>>();
        
        public Calc()
        {
            operators.Add("+", (a, b) => a + b);
            operators.Add("-", (a, b) => a - b);
            operators.Add("/", (a, b) => a / b);
            operators.Add("*", (a, b) => a * b);
        }

        public bool ContainsOperator(string a)
        {
            return operators.ContainsKey(a);
        }
        
        public bool TryCalculate(string Operator, out double ret, double a, double b)
        {
            Func<double, double, double> calFunc;
            if (operators.TryGetValue(Operator, out calFunc))
            {
                ret = calFunc(a, b);
                if (!double.IsNaN(ret) && !double.IsInfinity(ret)) return true;
                ret = default (double);
                return false;
            }
            ret = default (double);
            return false;
        }
    }
}