﻿namespace task1
{
    public interface IComandItem
    {
        bool CheckBufferOnComand(string buffer);
        bool TryRun(ref string readBuffer, ref CommandLineInterface.State stateMachin, App application);
    }
}