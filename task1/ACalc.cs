﻿namespace task1
{
    public interface ICalc
    {
        bool TryCalculate(string Operator, out double ret, double a, double b);
        bool ContainsOperator(string a);
    }
}