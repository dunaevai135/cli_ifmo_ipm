﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace task1.Comands
{
    public class Load : IComandItem
    {
        public bool CheckBufferOnComand(string buffer)
        {
            return buffer == "l";
        }

        public bool TryRun(ref string readBuffer, ref CommandLineInterface.State stateMachin,
            App application)
        {
            var operands = new List<double>();
            var answrers = new List<double>();
            var operators = new List<string>();
            
            var calcIO = new ConsoleCalcIO();
            calcIO.Write("Enter path to load:");
            var loadPath = calcIO.ReadString();
            
            StreamReader sr = new StreamReader(loadPath);
            
            string input;
            string pattern = @"^...?\[(?<index>[-\d]+)\]:=( Out\[-1\] (?<operator>.+)){0,1} (?<dig>[-\d\.eE]+)$";
            Regex rgx = new Regex(pattern);
            int index = 0;
            
            while (sr.Peek() >= 0)
            {
                input = sr.ReadLine();
                MatchCollection matches = rgx.Matches(input);
                if (matches.Count != 1)
                {
                    return false;
                }
                GroupCollection groups = matches[0].Groups;
                if (groups.Count < 2)
                {
                    return false;
                }
                
                if (index % 2 == 0)
                {
                    calcIO.WriteLine("In[" + groups["index"] + "] := " + groups["operator"] + " " + groups["dig"]);
                    
                    operands.Add(double.Parse(groups["dig"].Value));
                    if (groups["operator"].Success)
                    {
                        operators.Add(groups["operator"].Value);
                    }
                    //TODO check index
                }
                else
                {
                    calcIO.WriteLine("Out[" + groups["index"] + "] :=  " + groups["dig"]);
                    answrers.Add(double.Parse(groups["dig"].Value));
                    
                }

                index++;
            }
            sr.Close();

            application.Operands = operands;
            application.Answrers = answrers;
            application.Operators = operators;

            return true;
        }
    }
}