﻿using System;
using System.Collections.Generic;

namespace task1.Comands
{
    public class ComandListInitializer
    {
        public List<IComandItem> comands;
        
        public ComandListInitializer()
        {
            comands = new List<IComandItem> {new Save(), new Load(), new Quit(), new SetAnsver()};
        }
    }
}