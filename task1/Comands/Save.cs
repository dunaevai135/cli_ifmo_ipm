﻿using System;
using System.IO;

namespace task1.Comands
{
    public class Save : IComandItem
    {
        public bool CheckBufferOnComand(string buffer)
        {
            return buffer == "s";
        }

        public bool TryRun(ref string readBuffer, ref CommandLineInterface.State stateMachin,
            App application)
        {
            var calcIO = new ConsoleCalcIO();
            calcIO.Write("Enter path to save:");
            var savePath = calcIO.ReadString();

            string text = "In[0]:= "+application.Operands[0]+"\n" +
                          "Out[0]:= "+application.Answrers[0];
            
            try
            {
                using (StreamWriter sw = new StreamWriter(savePath, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(text);
                    for (int i = 0; i < application.Operators.Count; i++)
                    {
                        sw.WriteLine("In["+ (i+1) +"]:= Out[-1] "+ application.Operators[i] + " " +application.Operands[i+1]);
                        sw.WriteLine("Out["+ (i+1) +"]:= "+application.Answrers[i+1]);
                    }
                }
            }
            catch (Exception e)
            {
                calcIO.WriteLine(e.Message);
            }

            return true;
        }
    }
}