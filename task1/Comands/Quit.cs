﻿namespace task1.Comands
{
    public class Quit : IComandItem
    {
        public bool CheckBufferOnComand(string buffer)
        {
            return buffer == "q";
        }

        public bool TryRun(ref string readBuffer, ref CommandLineInterface.State stateMachin,
            App application)
        {
            stateMachin = CommandLineInterface.State.Exit;
            return true;
        }
    }
}