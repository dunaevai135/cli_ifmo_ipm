﻿namespace task1.Comands
{
    public class SetAnsver : IComandItem
    {
        public bool CheckBufferOnComand(string buffer)
        {
            if (buffer.Length == 0)
                return false;
            return buffer[0] == '#';
        }

        public bool TryRun(ref string readBuffer, ref CommandLineInterface.State stateMachin,
            App application)
        {
            readBuffer = readBuffer.Substring(1, readBuffer.Length - 1);
            stateMachin = CommandLineInterface.State.SetAnsver;
            return true;
        }
    }
}